/*! Real hardware support */

#![no_std]
use apps;
use libtock2::console;
use libtock2::gnss::GNSS;
use libtock2::platform::ErrorCode;
use libtock2::screen::Screen;
use ray_graphics::{Monochrome, TiledRenderer, Image, PixelStore, Scene};
use ray_graphics::egc::{
    geometry::Size,
    prelude::OriginDimensions,
    primitives::Rectangle,
};

pub struct Gnss;

impl apps::Gnss for Gnss {
    type Error = ErrorCode;
    fn read(&mut self, buf: &mut [u8]) -> (usize, Result<(), <Self as apps::Gnss>::Error>) {
        GNSS::read(buf)
    }
}

pub struct BufDisp(pub Image<4096>);

impl apps::Display for BufDisp {
    type Error = ErrorCode;
    fn flush(&mut self) -> Result<(), libtock2::platform::ErrorCode> {
        Screen::write(&self.0.get_buf())
    }
}

impl OriginDimensions for BufDisp {
    fn size(&self) -> Size {
        let (width, height) = self.0.get_size();
        Size {width: width as u32, height: height as u32}
    }
}

impl TiledRenderer for BufDisp {
    type Color = ray_graphics::Monochrome;
    type Error = core::convert::Infallible;
    fn fill<S: Scene<ray_graphics::Monochrome>>(&mut self, area: &Rectangle, scene: S)
        -> Result<(), <Self as TiledRenderer>::Error>
    {
        self.0.fill(area, scene)
    }
}

impl PixelStore<ray_graphics::Monochrome> for BufDisp {
    fn set(&mut self, x: usize, y: usize, color: Monochrome) {
        self.0.set(x, y, color)
    }
    fn get_size(&self) -> (usize, usize) {
        self.0.get_size()
    }
}

pub struct Console;

impl apps::Console for Console {
    type Writer = console::ConsoleWriter;
    fn writer(&self) -> Self::Writer {
        console::Console::writer()
    }
}