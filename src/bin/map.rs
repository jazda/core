//! Prints out GNSS stream

#![no_main]
#![no_std]
use apps::map;

use jazda_core::{BufDisp, Gnss};

use libtock2::platform::ErrorCode;
use libtock2::runtime::{set_main, stack_size};
use libtock2::screen::Screen;

use ray_graphics::{Image};

set_main! {main}
stack_size! {0x2800}

fn main() {
    draw().unwrap()
}

fn draw() -> Result<(), ErrorCode> {
    Screen::set_power(true).unwrap();

    let display = BufDisp(Image::new(176, 176));

    map::run(Gnss, display)
}
