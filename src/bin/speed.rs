//! Prints out GNSS stream

#![no_main]
#![no_std]
use apps::speed;

use core::fmt::Write;

use jazda_core::{BufDisp, Console, Gnss};

use libtock2::console;
use libtock2::runtime::{set_main, stack_size};
use libtock2::screen::Screen;

use ray_graphics::{Image, Monochrome, PixelStore};
use ray_graphics::egc::geometry::Dimensions;

set_main! {main}
stack_size! {0x1800}

fn main() {
    loop {
    draw().unwrap()
    }
}

fn draw() -> Result<(), speed::Error> {
    Screen::set_power(true).unwrap();

    let display = BufDisp(Image::new(176, 176));

    if let Err(e) = speed::run(Console, Gnss, display) {
        assert!(writeln!(console::Console::writer(), "{:?}", e).is_ok());
        let mut display = BufDisp(Image::new(20, 20));
        display.set_area(display.bounding_box(), Monochrome::Dark);
        
        for i in 0..16 {
            display.set(
                0,
                i,
                if e.line & (1 << i) == 1 {
                    Monochrome::Dark
                } else {
                    Monochrome::Light
                },
            )
        }
    }
    Ok(())
}
