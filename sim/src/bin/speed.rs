use embedded_graphics_simulator::{SimulatorDisplay, Window, OutputSettingsBuilder};
use apps::speed;
use sim;
use sim::{ErrorCode, MonoTranslate};
use ray_graphics::egc::{
    pixelcolor::BinaryColor,
    prelude::Size,
};

fn main() {
    dbg!(m().unwrap());
}

fn m() -> Result<(), speed::Error> {
    let gnss = sim::Gnss;

    let display = SimulatorDisplay::<BinaryColor>::new(Size::new(176, 176));
    let display = MonoTranslate{ disp: display };

    let output_settings = OutputSettingsBuilder::new()
        .scale(2)
        .build();
    let w = Window::new("Speed test", &output_settings);
    let display = sim::Display { w, buffer: display };
    speed::run(sim::Console, gnss, display)
}