use apps::Display;
use core::fmt::Debug;
use embedded_graphics_simulator::{SimulatorDisplay, Window, OutputSettingsBuilder};
use ray_graphics::{Fixed, Monochrome, PixelStore, Primitive, Scene, Stack, TiledRenderer};
use ray_graphics::digits;
use ray_graphics::sdf::{scale, translate};
use sim;
use sim::{MonoTranslate};
use std::thread;
use std::time::Duration;
use ray_graphics::egc::{
    geometry::Dimensions,
    pixelcolor::BinaryColor,
    prelude::{Point, Size},
};

fn main() {
    let display = SimulatorDisplay::<BinaryColor>::new(Size::new(176, 176));
    let display = MonoTranslate{ disp: display };

    let output_settings = OutputSettingsBuilder::new()
        .scale(2)
        .build();
    let w = Window::new("counting test", &output_settings);
    let display = sim::Display { w, buffer: display };
    run(display).unwrap()
}

struct Scale<T: Scene<Monochrome>>{
    scene: T,
    factor: Fixed,
}

impl <T: Scene<Monochrome>> Scene<Monochrome> for Scale<T> {
    fn get_color(&self, p: Point) -> Monochrome {
        let Point { x, y } = p;
        self.scene.get_color(Point {
            x: self.factor.mul(x).round(),
            y: self.factor.mul(y).round(),
        })
    }
}

macro_rules! bake {
    ($f:expr => $($arg:expr),* $(,)?) => {
        move |point: ray_graphics::egc::prelude::Point| {
            $f(point $(,$arg)*)
        }
    }
}

macro_rules! to_scene {
    ($f:path : ($($arg:expr)*$(,)?)) => {
    ray_graphics::F(move |point: ray_graphics::egc::prelude::Point| {
            if $f(point $(,$arg)*) { Monochrome::Light }
            else { Monochrome::Dark }
        })
    }
}

fn to_dark(v: bool) -> Monochrome {
    if v { Monochrome::Dark }
    else { Monochrome::Light }
}

pub fn run<E, D>(mut display: D) -> Result<(), E> 
where
    E: Debug,
    D: Display<Error=E>
        + Dimensions
        + PixelStore<Monochrome>
        + TiledRenderer<Color=Monochrome>,
    <D as TiledRenderer>::Error: Debug,
{
    loop {
        for i in 0..256000 {
            let s = format!("{}", i % 256);
            let s = &s;
            let digits = bake!(digits::st => &s);
            let scaled = bake!(scale => digits, Fixed::one().mul(128) / (16 + i % 128));
            let moved = bake!(
                translate => scaled, Point { x: (i / 2) % 176, y: i % 176}
            );
            //let scene = to_scene!(digits::st : (&s));
            let scene = ray_graphics::F(|x| to_dark(moved(x)));
            display.fill(&display.bounding_box(), scene).unwrap();
            display.flush()?;
            thread::sleep(Duration::from_millis(200));
        }
    }
}