use core::fmt;
use embedded_graphics_simulator::{SimulatorDisplay, SimulatorEvent, Window};
use ray_graphics::{ TiledRenderer, Monochrome, PixelStore, Scene};
use ray_graphics::egc::{
    Pixel,
    draw_target::DrawTarget as _,
    pixelcolor::BinaryColor,
    prelude::{OriginDimensions, Point, PointsIter, Size},
    primitives::Rectangle,
};
use std::io::{stdout, Write};

pub struct MonoTranslate {
    pub disp: SimulatorDisplay<BinaryColor>,
}

impl OriginDimensions for MonoTranslate {
    fn size(&self) -> Size {
        self.disp.size()
    }
}

impl TiledRenderer for MonoTranslate {
    type Color = Monochrome;
    type Error = core::convert::Infallible;
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error> {
        for point in area.points() {
            let color = match scene.get_color(point) {
                Monochrome::Light => BinaryColor::On,
                Monochrome::Dark => BinaryColor::Off,
            };
            self.disp.draw_iter([Pixel(point, color)])?;
        }
        Ok(())
    }
}

fn refresh_disp(w: &mut Window, disp: &SimulatorDisplay::<BinaryColor>) {
    w.update(disp);

    if w.events().any(|e| e == SimulatorEvent::Quit) {
        panic!()
    }
}

/// Basically a copy of the one in libtock2. Maybe useful.
#[allow(non_snake_case)]
mod GNSS {
    use super::ErrorCode;
    use std::io::Read;
    use std::time::Duration;
    use std::thread;

    pub fn read(mut buf: &mut [u8]) -> (usize, Result<(), ErrorCode>) {
        let mut stdin = std::io::stdin();
        let res = stdin.read(&mut buf);
        // Simulate delay between points
        thread::sleep(Duration::from_millis(50));
        match res {
            Ok(n) => (n, Ok(())),
            Err(_) => (0, Err(ErrorCode::Fail)),
        }
    }
}

/// Simulated GNSS
pub struct Gnss;

impl apps::Gnss for Gnss {
    type Error = ErrorCode;
    fn read(&mut self, buf: &mut [u8]) -> (usize, Result<(), ErrorCode>) {
        GNSS::read(buf)
    }
}

/// Simulated display
pub struct Display {
    pub w: Window,
    pub buffer: MonoTranslate,
}

impl apps::Display for Display {
    type Error = ErrorCode;
    fn flush(&mut self) -> Result<(), Self::Error> {
        refresh_disp(&mut self.w, &self.buffer.disp);
        Ok(())
    }
}

impl TiledRenderer for Display {
    type Color = Monochrome;
    type Error = core::convert::Infallible;
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error> {
        self.buffer.fill(area, scene)
    }
}

impl PixelStore<Monochrome> for Display {
    fn set(&mut self, x: usize, y: usize, color: Monochrome) { 
        let color = match color {
            Monochrome::Light => BinaryColor::On,
            Monochrome::Dark => BinaryColor::Off,
        };
        self.buffer.disp.draw_iter(
            [Pixel(Point { x: x as i32, y: y as i32 }, color)],
        ).unwrap();
    }
    fn get_size(&self) -> (usize, usize) {
        let Size { width, height} = self.size();
        (width as usize, height as usize)
    }
}
impl OriginDimensions for Display {
    fn size(&self) -> Size {
        self.buffer.disp.size()
    }
}

// FIXME: use libtock's
#[derive(Debug)]
pub enum ErrorCode {
    Fail,
}

pub struct Console;

impl apps::Console for Console {
    type Writer = Writer;
    fn writer(&self) -> Writer {
        Writer
    }
}

pub struct Writer;

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
        write!(stdout(), "{}", s).map_err(|_| fmt::Error::default())
    }
}