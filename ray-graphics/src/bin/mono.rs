use arrayvec::ArrayString;
use embedded_graphics::{
    pixelcolor::BinaryColor,
    primitives::{Circle, Rectangle},
    mono_font::{ascii::FONT_6X9},
};
use embedded_graphics::geometry::Dimensions;
use embedded_graphics_core as egc;
use egc::Pixel;
use egc::prelude::{OriginDimensions, Point, PointsIter, Size};
use embedded_graphics_simulator::{SimulatorDisplay, Window, OutputSettingsBuilder};

use ray_graphics::{ DrawTarget, Image, Label, Monochrome, Primitive, Scene, Stack, TwoLevel};
use ray_graphics::constrig::{Fixed};
use ray_graphics::painters::Track;

use embedded_graphics::draw_target::DrawTarget as _;

struct MonoTranslate {
    disp: SimulatorDisplay<BinaryColor>,
}

impl OriginDimensions for MonoTranslate {
    fn size(&self) -> Size {
        self.disp.size()
    }
}

impl DrawTarget for MonoTranslate {
    type Color = Monochrome;
    type Error = core::convert::Infallible;
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error> {
        for point in area.points() {
            let color = match scene.get_color(point) {
                Monochrome::Light => BinaryColor::On,
                Monochrome::Dark => BinaryColor::Off,
            };
            self.disp.draw_iter([Pixel(point, color)])?;
        }
        Ok(())
    }
}

/// tan angle: closer to 0 is closer to vertical, 256=1,
/// grows into positive x
// Float really needed for the angle
fn plane(x: i32, y: i32, tan_angle: i32) -> bool {
    x * 256 <= tan_angle * y
}

/// Tan of half the angle.
fn angle(x: i32, y: i32, tan_half_angle: i32) -> bool {
    plane(x, y, tan_half_angle) && plane(x, -y, tan_half_angle)
}

fn rotate(angle: Fixed, point: Point) -> Point {
    let s = angle.sin();
    let c = angle.cos();
    Point {
        x: c.mul(point.x).add(s.mul(point.y)).round(),
        y: c.mul(point.y).sub(s.mul(point.x)).round(),
    }
}

fn translate(point: Point, x: i32, y: i32) -> Point {
    Point {
        x: point.x - x,
        y: point.y - y,
    }
}

/// This actually works kinda nice
/// It needs rotation, but rotation sucks.
/// Better use circle & 2 planes
fn arc(point: Point) -> Monochrome {   
    let a = point.x;
    let b = point.y;
    let circle = a*a + b*b < 170;

    let half_tan = Fixed::TAU.div(2).tan().0;
    let a = angle(a, b, half_tan);
    if circle && a { Monochrome::Light }
    else { Monochrome::Dark }
}

fn rotarc(point: Point) -> Monochrome {
    arc(rotate(Fixed(270), translate(point, 60, 30)))
}

/// Arc and two planes
fn arc_gauge(point: Point) -> Monochrome {
    let angle = Fixed::TAU.div(3);
    let point = translate(point, 90, 30);
    let x = point.x;
    let y = point.y;
    let circle = x*x + y*y < 200;
    let top = y <= 0;
    let rplane = rotate(angle, point);
    let p = rplane.y >= 0;
    // only half area: let p = !plane(-x, y, 9990);
    if circle && top && p { Monochrome::Light }
    else { Monochrome::Dark }
}


fn main() -> Result<(), core::convert::Infallible> {
    let display = SimulatorDisplay::<BinaryColor>::new(Size::new(128, 64));

    let mut display = MonoTranslate{ disp: display };
    let mut scene = Stack([(); 8].map(|()| Primitive::Dark));
    scene.0[0] = Primitive::Circle(Circle::with_center(Point{ x: 20, y: 30 }, 7));
    scene.0[1] = Primitive::Circle(Circle::with_center(Point{ x: 30, y: 30 }, 17));
    scene.0[2] = Primitive::Sdf(&rotarc);
    scene.0[3] = Primitive::Text(Label {
        text: ArrayString::<8>::from("32.7").unwrap(),
        font: FONT_6X9,
    });
    scene.0[4] = Primitive::Sdf(&arc_gauge);
    display.fill(&display.bounding_box(), scene).unwrap();
    let mut buf = Image::<4096>::new(176, 176);
    let bounds = Rectangle {
        top_left: Point { x: 0, y: 0},
        size: Size { width: 100, height: 64 },
    };
    scene.draw(&mut buf);
    display.fill(&display.bounding_box(), buf).unwrap();
    /*
    let line_style = PrimitiveStyle::with_stroke(BinaryColor::On, 1);
    let text_style = MonoTextStyle::new(&FONT_6X9, BinaryColor::On);

    Circle::new(Point::new(72, 8), 48)
        .into_styled(line_style)
        .draw(&mut display)?;

    Line::new(Point::new(48, 16), Point::new(8, 16))
        .into_styled(line_style)
        .draw(&mut display)?;

    Line::new(Point::new(48, 16), Point::new(64, 32))
        .into_styled(line_style)
        .draw(&mut display)?;

    Rectangle::new(Point::new(79, 15), Size::new(34, 34))
        .into_styled(line_style)
        .draw(&mut display)?;

    Text::new("Hello World!", Point::new(5, 5), text_style).draw(&mut display)?;*/

    let output_settings = OutputSettingsBuilder::new()
        .scale(5)
        .build();
    Window::new("Hello World", &output_settings).show_static(&display.disp);

    Ok(())
}