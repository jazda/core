/*! Const fixed point trigonometry. To (pre)calculate values.
 */

/// A fixed point number.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct Fixed(pub i32);

impl Fixed {
    const ONE: i32 = 512;
    pub const TAU: Fixed = Fixed(3217);
    pub const fn one() -> Self {
        Fixed(Self::ONE)
    }
    
    pub const fn floor(self) -> i32 {
        self.0 / Self::ONE
    }
    pub const fn round(self) -> i32 {
        let d = self.0 / Self::ONE;
        if self.0 % Self::ONE > Self::ONE / 2 {
            d + 1
        } else {
            d
        }
    }
    // Why are all the ops here?
    // Because ops aren't const, and we *want* const.
    pub const fn mul(self, rhs: i32) -> Fixed {
        Self(self.0 * rhs)
    }
    
    pub const fn mulf(self, rhs: Fixed) -> Fixed {
        Self(self.0 * rhs.0 / Self::ONE)
    }
    
    pub fn mulf32(self, rhs: f32) -> f32 {
        self.0 as f32 * rhs / Self::ONE as f32
    }
    
    pub const fn div(self, rhs: i32) -> Fixed {
        Self(self.0 / rhs)
    }
    
    pub const fn divf(self, rhs: Fixed) -> Fixed {
        Self(self.0 / rhs.0 * Self::ONE)
    }

    pub const fn sub(self, rhs: Fixed) -> Fixed {
        Self(self.0 - rhs.0)
    }
    
    pub const fn add(self, rhs: Fixed) -> Fixed {
        Self(self.0 + rhs.0)
    }
    
    pub const fn neg(self) -> Fixed {
        Self(-self.0)
    }
    
    // Limited to 0..TAU
    const fn _sin(self) -> Fixed {
        let x = if self.0 > Self::TAU.0 / 4 {
            Self::TAU.div(2).sub(self)
        } else {
            self
        };

        let x2 = x.mulf(x);
        let x3 = x.mulf(x2);
        let x5 = x.mulf(x3);
        //let x7 = x.mulf(x5);
        
        x.sub(x3.div(6)).add(x5.div(120)) //.sub(x7.div(5040))
    }
    
    /// Rather inaccurate. Enough for drawing.
    pub const fn sin(self) -> Fixed {
        let x = Self(self.0.rem_euclid(Self::TAU.0));
        if x.0 > Self::TAU.0 / 2 {
            x.sub(Self::TAU.div(2))
                ._sin()
                .neg()
        } else {
            x._sin()
        }
    }
    
    pub const fn cos(self) -> Fixed {
        self.add(Self::TAU.div(4)).sin()
    }

    pub const fn tan(self) -> Fixed {
        self.sin().divf(self.cos())
    }
}

// Ops are a bonus.
use core::ops;

impl ops::Mul<Fixed> for Fixed {
    type Output = Fixed;
    fn mul(self, rhs: Fixed) -> Self::Output {
        self.mulf(rhs)
    }
}

impl ops::Div<i32> for Fixed {
    type Output = Fixed;
    fn div(self, rhs: i32) -> Self::Output {
        self.div(rhs)
    }
}

impl ops::Div<Fixed> for Fixed {
    type Output = Fixed;
    fn div(self, rhs: Fixed) -> Self::Output {
        self.divf(rhs)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[allow(dead_code)]
    const A: Fixed = Fixed(1).mul(9);
    #[allow(dead_code)]
    const B: Fixed = Fixed(1).mulf(Fixed(0));

    #[test]
    fn mul() {
        assert_eq!(Fixed::one().mulf(Fixed::one()), Fixed::one());
    }

    #[test]
    fn div() {
        assert_eq!(Fixed::one().div(4), Fixed(Fixed::ONE / 4));
        assert_eq!(Fixed::one().divf(Fixed::one()), Fixed::one());
    }
    
    #[test]
    fn sin() {
        assert!(
            Fixed::one().sin() == Fixed(431)
            || Fixed::one().sin() == Fixed(430)
        );
        assert_eq!(Fixed(0).sin(), Fixed(0));
        
        assert!(Fixed::TAU.div(4).sin() <= Fixed::one());
        assert!(Fixed::TAU.div(4).sin() > Fixed::one().mul(62).div(64));
        assert!(Fixed::TAU.div(4).neg().sin() >= Fixed::one().neg());
        assert!(Fixed::TAU.div(4).neg().sin() < Fixed::one().mul(62).div(64).neg());
        assert_eq!(Fixed::TAU.div(2)._sin(), Fixed(0));
        assert_eq!(Fixed::TAU.div(2).sin(), Fixed(0));
        assert_eq!(Fixed::TAU.sin(), Fixed(0));
    }
    
    #[test]
    fn cos() {
        assert!(
            Fixed::one().cos() == Fixed(276)
            || Fixed::one().cos() == Fixed(277)
        );
        assert!(Fixed(0).cos() <= Fixed::one());
        assert!(Fixed(0).cos() > Fixed::one().mul(62).div(64));
        assert_eq!(Fixed::one().cos(), Fixed::one().neg().cos());
    }

    #[test]
    fn tan() {
        assert_eq!(Fixed(0).tan(), Fixed(0));
        assert_eq!(Fixed::TAU.tan(), Fixed(0));
        assert_eq!(Fixed::TAU.div(2).tan(), Fixed(0));
        //assert_eq!(Fixed::TAU.div(4).tan(), Fixed(0));
    }
}