use crate::Fixed;
use embedded_graphics_core::prelude::Point;

pub mod point {
    use super::*;
    pub fn scale(point: Point, s: Fixed) -> Point {
        let Point {x, y} = point;
        Point { x: s.mul(x).round(), y: s.mul(y).round() }
    }
    
    pub fn translate(point: Point, offset: Point) -> Point {
        point - offset
    }
}

pub fn scale(point: Point, f: impl Fn(Point) -> bool, s: Fixed) -> bool {
    f(point::scale(point, s))
}

pub fn translate(point: Point, f: impl Fn(Point) -> bool, offset: Point) -> bool {
    f(point::translate(point, offset))
}
