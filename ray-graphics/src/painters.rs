/*! Procedures for drawing into buffers.

Those are less powerful than SDFs, because they don't compose so well.
Those also require buffer allocation.
On the other hand, they can be implemented in a more efficient way.

It's actually quite an awful API, in comparison to embedded-graphics.
It might make sense if the compiled size is smaller.
*/
// FIXME: how to deal with negative positions?
// Add negatives to PixelStore?
// Add translation to draw()?


use super::PixelStore;
use super::Point;
use arrayvec::ArrayVec;
use bresenham::Bresenham;
use core::cmp;
use embedded_graphics_core::primitives::Rectangle;
use embedded_graphics_core::prelude::Size;
use float_ord::FloatOrd;


/// Can draw pixels on top of an image
pub trait Painter {
    fn draw<T: Copy, I: PixelStore<T>>(&self, image: &mut I, color: T);
}

impl<T> Painter for &T where T: Painter {
    fn draw<C: Copy, I: PixelStore<C>>(&self, image: &mut I, color: C) {
        (*self).draw(image, color)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Line {
    pub start: Point,
    pub end: Point,
}

impl Painter for Line {
    fn draw<T: Copy, I: PixelStore<T>>(&self, image: &mut I, color: T) {
        let points = Bresenham::new(
            (self.start.x as isize, self.start.y as isize),
            (self.end.x as isize, self.end.y as isize),
        );
        for (x, y) in points {
            image.set(x as usize, y as usize, color);
        }
    }
}


#[derive(Clone, Debug)]
pub struct TrackPoints {
    points: ArrayVec<(f32, f32), 100>,
    /// Approximate track length. Doesn't need to be precise,
    /// only decides if points are added or replaced.
    len: f32,
}


/// A vertex-limited isotropically resizeable polyline
/// Stores points represented in a certain projection.
/// Transform will get applied to them while drawing.
#[derive(Clone, Debug)]
pub struct Track {
    pub points: TrackPoints,
    /// xoffset, yoffset, scale
    pub transform: ((f32, f32), f32),
}

impl Track {
    
    fn transform_from_corners(
        (min_x, min_y): (f32, f32),
        (max_x, max_y): (f32, f32),
        bounds: Rectangle) -> ((f32, f32), f32)
    {
        use FloatOrd as f;
        let Size {width, height } = bounds.size;
        let xscale = width as f32 / (max_x - min_x);
        let yscale = height as f32 / (max_y - min_y);
        let scale = cmp::min(f(xscale), f(yscale)).0;
        let offset = (min_x * scale, min_y * scale);
        (offset, scale)
    }
    
    /// would adding this line segment change the display?
    pub fn would_cause_change(&self, new: (f32, f32)) -> bool {
        let last = self.points.points[self.points.points.len()];
        let last = Self::apply_transform(self.transform, last);
        let new = Self::apply_transform(self.transform, new);
        last != new
    }

    fn invert_transform(
        ((xo, yo), s): ((f32, f32), f32),
        (x, y): (i32, i32),
    ) -> (f32, f32) {
        ((x as f32 + xo) / s, (y as f32 + yo) / s)
    }
    
    fn apply_transform(
        ((xo, yo), s): ((f32, f32), f32),
        (x, y): (f32, f32),
    ) -> (i32, i32) {
        ((x * s - xo) as i32, (y * s - yo) as i32)
    }
}

impl Painter for Track {
    fn draw<T: Copy, I: PixelStore<T>>(&self, image: &mut I, color: T) {
        let (mut sx, mut sy)
            = Self::apply_transform(self.transform, self.points.points[0]);
        for (ex, ey) in self.points.points[1..].iter()
            .map(|p| Self::apply_transform(self.transform, *p))
        {
            Line {
                start: Point { x: sx, y: sy },
                end: Point { x: ex, y: ey },
            }
            .draw(image, color);
            sx = ex;
            sy = ey;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn tf() {
        assert_eq!(
            Track::transform_from_corners(
                (0.0, 0.0), (10.0, 10.0),
                Rectangle {
                    top_left: Point { x: 0, y: 0 },
                    size: Size { width: 100, height: 50 }
                },
            ),
            ((0.0, 0.0), 5.0)
        );
    }
    #[test]
    fn ugh() {
        let t = Track::new(
            (0.0, 0.0), (10.0, 10.0),
            Rectangle {
                top_left: Point { x: 0, y: 0 },
                size: Size { width: 100, height: 50 }
            }
        );
        assert_eq!(t.transform.1, 5.0);
    }
}