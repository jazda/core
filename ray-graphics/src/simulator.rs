use super::{ TiledRenderer, Monochrome, Scene };

use embedded_graphics_core as egc;
use egc::Pixel;
use egc::prelude::{ OriginDimensions, PointsIter, Size };
use embedded_graphics_core::primitives::Rectangle;

pub struct Simulator<D: egc::prelude::DrawTarget> {
    device: D,
}

impl<D: egc::prelude::DrawTarget> Simulator<D> {
    pub fn new(device: D) -> Self {
        Self { device }
    }
}

impl<D: egc::prelude::DrawTarget> OriginDimensions for Simulator<D> {
    fn size(&self) -> Size {
        Size { width: 196, height: 196 }
    }
}

impl<D: egc::prelude::DrawTarget<Color=Monochrome, Error=()>> TiledRenderer for Simulator<D> {
    type Color = Monochrome;
    type Error = ();
    fn fill<S: Scene<Self::Color>>(&mut self, area: &Rectangle, scene: S) -> Result<(), Self::Error> {
        for point in area.points() {
            self.device.draw_iter([Pixel(point, scene.get_color(point))])?;
        }
        Ok(())
    }
}