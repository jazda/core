/*! Drawing digits implemented as SDFs */

use crate::{Fixed, Monochrome};
use crate::egc::primitives::Rectangle;
use crate::egc::prelude::{Point, Size};


/// All digits clipped to this size
const DIGIT_SIZE: Rectangle = Rectangle {
    top_left: Point { x: 0, y: 0 },
    size: Size {width: 40, height: 80 }
};


/// Circle with origin at 0
fn zcircle(point: Point, diameter: u32) -> bool {
    let rsquare = diameter * diameter / 4;
    let a = point.x;
    let b = point.y;
    rsquare >= (a * a) as u32 + (b * b) as u32
}

fn scale(point: Point, xs: Fixed, ys: Fixed) -> Point {
    let Point { x, y } = point;
    Point {
        x: xs.mul(x).round(),
        y: ys.mul(y).round(),
    }
}


fn line(point: Point, angle: Point, thickness: u32) -> bool {
    let k = angle.y*point.x - angle.x*point.y;
    let f = (k*k / (angle.x*angle.x+angle.y*angle.y)) as u32;
    f < thickness
}

pub(crate) mod uncut {
    use super::*;
    pub fn zero(point: Point) -> bool {
        let point = point - Point { x: 20, y: 40 };
        let point = scale(point, Fixed::one().mul(2), Fixed::one());
        zcircle(point, 79) & !zcircle(point, 61)
    }


    pub fn one(point: Point) -> bool {
        line(point - Point { x: 20, y: 0 }, Point { x: 20, y: -30 }, 8)
        || line(point - Point { x: 21, y: 0 }, Point { x: 0, y: 80 }, 6)
    }

    pub fn two(point: Point) -> bool {
    // horizontal
        point.y >= 72
        // slanted
        || line(point - Point { x: 0, y: 76 }, Point { x: 40, y: -54 }, 8)
        // hook
        || {
            let point = point - Point { x: 20, y: 20 };
            point.y <= 3 && zcircle(point, 41) & !zcircle(point, 29)
        }
    }

    pub fn three(point: Point) -> bool {
        let Point {x, y} = point;
        let point = Point {
            x,
            y: {
                if y < 34 { Fixed::one().mul(8).div(7).mul(y).round() }
                else { Fixed::one().mul(8).div(10).mul(80 - y).round() }
            },
        };
        let point = point - Point { x: 20, y: 20 };
        let ring = zcircle(point, 41) & !zcircle(point, 29);
        let plane = {
            point.y <= point.x * 2 + 20
        };
        ring && plane
    }


    pub fn four(point: Point) -> bool {
        // back
        line(point - Point { x: 30, y: 0 }, Point { x: 30, y: -48 }, 8)
        // vertical
        || line(point - Point { x: 30, y: 0 }, Point { x: 0, y: 80 }, 6)
        // horizontal
        || line(point - Point { x: 0, y: 50 }, Point { x: 40, y: 0 }, 8)
    }

    pub fn five(point: Point) -> bool {
        // horizontal
        point.y <= 8
        // back
        || (point.y < 38 && point.x < 5)
        // oval
        || {
            let point = point - Point { x: 14, y: 54 };
            zcircle(point, 51) & !zcircle(point, 38)
        }
    }

    pub fn lower_loop(point: Point) -> bool {
        let Point {x, y} = point;
        let point = Point {
            x: Fixed::one().mul(5).div(4).mul(x).round(),
            y,
        };

        let point = point - Point { x: 25, y: 55 };
        let ring = zcircle(point, 51) & !zcircle(point, 36);
        ring
    }

    pub fn six(point: Point) -> bool {
        let plane = {
            let point = point - Point { x: 50, y: 25 };
            point.x < -point.y - 20
        };
        plane && {
            let Point {x, y} = point;
            let point = Point {
                x: x * 2,
                y
            };
            let point = point - Point { x: 43, y: 43 };
            let ring = zcircle(point, 88) & !zcircle(point, 71);
            ring
        }
        || lower_loop(point)
    }

    pub fn seven(point: Point) -> bool {
        // back
        line(point - Point { x: 10, y: 80 }, Point { x: 30, y: -78 }, 8)
        // horizontal
        || point.y <= 8
    }

    pub fn eight(point: Point) -> bool {
        let ring = {
            let point = point - Point { x: 20, y: 18 };
            zcircle(point, 35) & !zcircle(point, 24)
        };
        ring || lower_loop(point)
    }

    pub fn nine(point: Point) -> bool {
        let Point {x, y} = point;
        let point = Point {
            x: 40 - x,
            y: 80 - y,
        };
        six(point)
}
}

fn digit(point: Point, offset: Point, digit: impl Fn(Point) -> bool) -> bool {
    let point = point - offset;
    DIGIT_SIZE.contains(point) && digit(point)
}

macro_rules! to_digit {
    ($d:ident) => {
        pub fn $d(point: Point, offset: Point) -> bool {
            digit(point, offset, uncut::$d)
        }
    }
}

to_digit!(zero);
to_digit!(one);
to_digit!(two);
to_digit!(three);
to_digit!(four);
to_digit!(five);
to_digit!(six);
to_digit!(seven);
to_digit!(eight);
to_digit!(nine);

pub const DIGITS: [fn(Point, Point) -> bool; 10] = [
    zero, one, two, three, four, five, six, seven, eight, nine,
];

pub fn to_scene(digit: impl Fn(Point, Point) -> bool, offset: Point)
    -> impl Fn(Point) -> Monochrome
{
    move |point| {
        if !digit(point, offset) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}

pub fn st(point: Point, s: &str) -> bool {
    s.chars()
        .enumerate()
        .find(|(i, c)| match c.to_digit(10) {
            Some(d) => DIGITS[d as usize](
                point,
                Point { x: *i as i32 * 44, y: 0 },
            ),
            None => false,
        })
        .is_some()
}


pub fn n(point: Point, digits: &[u8]) -> bool {
    digits.iter()
        .enumerate()
        .rev()
        .find(|(i, d)| {
            if **d < 10 {
                DIGITS[**d as usize](
                    point,
                    Point { x: *i as i32 * 44, y: 0 },
                )
            } else {
                false
            }
        })
        .is_some()
}


pub fn string<'a>(s: &'a str) -> impl Fn(Point) -> Monochrome + 'a {
    move |point| {
        if st(point, s) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}

pub fn num<'a>(s: &'a[u8]) -> impl Fn(Point) -> Monochrome + 'a {
    move |point| {
        if n(point, s) { Monochrome::Light }
        else { Monochrome::Dark }
    }
}