/*! Geographical tools */
use arrayvec::ArrayVec;
use core::cmp;
use embedded_graphics_core::geometry::Point;
use embedded_graphics_core::primitives::Rectangle;
use embedded_graphics_core::prelude::Size;
use float_ord::FloatOrd;
use ray_graphics::PixelStore;
use ray_graphics::painters::{Line, Painter};

#[cfg(feature="std")]
fn abs(v: f32) -> f32 {
    v.abs()
}

#[cfg(not(feature="std"))]
fn abs(sd: f32) -> f32 {
    // TODO: don't know how to enable the FPU.
    // Also Tock doesn't support saving FPU registers on context switch,
    // or any other mechanism for float computation.
   /* use core::arch::asm;
    unsafe {
        asm!(
            "vsqrt.f32 {0}, {0}",
            inout(sreg) sd,
        )
    }
    sd*/
    
    if sd > 0.0 { sd } else { -sd }
}

/// Who needs Euclid in Manhattan?
fn dist((x, y): (f32, f32), (x1, y1): (f32, f32)) -> f32 {
    let sd = (x - x1+y - y1);
    abs(sd)
}

const POINTS_COUNT: usize = 100;

#[derive(Clone, Debug)]
pub struct TrackPoints {
    points: ArrayVec<(f32, f32), POINTS_COUNT>,
    /// Approximate track length. Doesn't need to be precise,
    /// only decides if points are added or replaced.
    len: f32,
}

impl TrackPoints {
    fn new(s: (f32, f32), e: (f32, f32)) -> Self {
        let mut points = ArrayVec::new();
        points.push(s);
        points.push(e);
        Self {
            points,
            len: dist(s, e),
        }
    }
    fn push(&mut self, point: (f32, f32)) {
        let last_idx = self.points.len() - 1;
        let last = self.points[last_idx];
        let pre_last = self.points[last_idx - 1];
        
        let avg_distance = self.len / self.points.len() as f32;
        let new_dist = dist(last, point);
        if self.points.len() < POINTS_COUNT / 2 {
            // Not even half-full. Points can be added without worrying.
            // This helps average out initial glitches if they aren't in another country.
            self.points.push(point);
            self.len += new_dist;
        } else if
            // the distance did not change much from the last point
            avg_distance > new_dist
            // and the resulting segment won't be too long (prevent creeping)
            && avg_distance > dist(pre_last, point)
        {
            // replace last point
            // CAUTION: this condition above is a bit magical. Change params and line segments may get disproportionally stretched near ends due to pruning.
            // What this wins is that pruning doesn't calculate distances.
            self.points[last_idx] = point;
            let pre_last = self.points[last_idx - 1];
            self.len = self.len - dist(pre_last, last) + dist(pre_last, point);
        } else if self.points.len() < POINTS_COUNT {
            // distance over average and space available
            self.points.push(point);
            self.len += new_dist;
        } else {
            // no space: prune half
            // Assuming that points are added more or less equidistantly
            // (the filter in Track should help ensure that by erasing stops).

            // First point stays forever.
            for i in 1..self.points.len() {
                self.points[(i + 1)/ 2] = self.points[i];
            }
            // length = POINTS_COUNT
            const LAST_IDX: usize = POINTS_COUNT - 1;
            const NEW_LAST_IDX: usize = (LAST_IDX + 1) / 2;
            self.points.truncate(NEW_LAST_IDX + 1);
            
            // Actual track length will most likely shorten due to the intermediate points taking some details with them. That's okay, we're interested in an approximation, and this is a hot-ish loop, so we can prioritize speed and drop the hundred potentially expensive calculations (sqrt in Euclidean) completely.
            
            self.points.push(point);
            self.len += new_dist;
        }
    }
}

/// A vertex-limited isotropically resizeable polyline
/// Stores points represented in a certain projection.
/// Transform will get applied to them while drawing.
#[derive(Clone, Debug)]
pub struct Track {
    pub points: TrackPoints,
    /// cached values
    min: (f32, f32),
    max: (f32, f32),
    /// Helps drop points which don't contribute to drawing
    bounds: Rectangle,
}

impl Track {
    pub fn new((sx, sy): (f32, f32), (ex, ey): (f32, f32), bounds: Rectangle) -> Self {
        use FloatOrd as f;
        let sx = f(sx);
        let sy = f(sy);
        let ex = f(ex);
        let ey = f(ey);
        let min_x = cmp::min(sx, ex).0;
        let max_x = cmp::max(sx, ex).0;
        let min_y = cmp::min(sy, ey).0;
        let max_y = cmp::max(sy, ey).0;

        Self {
            points: TrackPoints::new((sx.0, sy.0), (ex.0, ey.0)),
            min: (min_x, min_y),
            max: (max_x, max_y),
            bounds,
        }
    }
    
    fn transform(&self) -> ((f32, f32), f32) {
        Self::transform_from_corners(
            self.min,
            self.max,
            self.bounds,
        )
    }
    
    fn transform_from_corners(
        (min_x, min_y): (f32, f32),
        (max_x, max_y): (f32, f32),
        bounds: Rectangle,
    ) -> ((f32, f32), f32) {
        use FloatOrd as f;
        let Size { width, height } = bounds.size;
        if max_x - min_x == 0.0 && max_y - min_y == 0.0 {
            ((min_x * 10000000.0, min_y * 10000000.0), 10000000.0)
        } else {
            let xscale = width as f32 / (max_x - min_x);
            let yscale = height as f32 / (max_y - min_y);
            let scale = cmp::min(f(xscale), f(yscale)).0;
            let offset = (min_x * scale, min_y * scale);
            (offset, scale)
        }
    }
    
    /// would adding this line segment change the display?
    pub fn would_cause_change(&self, new: (f32, f32)) -> bool {
        let last = self.points.points[self.points.points.len()];
        let last = Self::apply_transform(self.transform(), last);
        let new = Self::apply_transform(self.transform(), new);
        last != new
    }

    /// Adds a point and recalculates transform to make output fit within bounds, while preserving aspect ratio.
    /// Must be called for each point (FIXME: make fields private)
    pub fn add_resize(&mut self, x: f32, y: f32, bounds: Rectangle) {    
        let Point { x: max_x, y: max_y } = bounds.bottom_right().unwrap();
        let Point { x: min_x, y: min_y } = bounds.top_left;
        let (max_x, max_y) = self.max;
        let (min_x, min_y) = self.min;
        use FloatOrd as f;
        let min_x = cmp::min(f(min_x), f(x)).0;
        let max_x = cmp::max(f(max_x), f(x)).0;
        let min_y = cmp::min(f(min_y), f(y)).0;
        let max_y = cmp::max(f(max_y), f(y)).0;

        self.min = (min_x, min_y);
        self.max = (max_x, max_y);
        self.points.push((x, y));
    }
    
    fn invert_transform(
        ((xo, yo), s): ((f32, f32), f32),
        (x, y): (i32, i32),
    ) -> (f32, f32) {
        ((x as f32 + xo) / s, (y as f32 + yo) / s)
    }
    
    fn apply_transform(
        ((xo, yo), s): ((f32, f32), f32),
        (x, y): (f32, f32),
    ) -> (i32, i32) {
        ((x * s - xo) as i32, (y * s - yo) as i32)
    }
}

impl Painter for Track {
    fn draw<T: Copy, I: PixelStore<T>>(&self, image: &mut I, color: T) {
        let transform = self.transform();
        let (mut sx, mut sy)
            = Self::apply_transform(transform, self.points.points[0]);
        for (ex, ey) in self.points.points[1..].iter()
            .map(|p| Self::apply_transform(transform, *p))
        {
            Line {
                start: Point { x: sx, y: sy },
                end: Point { x: ex, y: ey },
            }
            .draw(image, color);
            sx = ex;
            sy = ey;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn tf() {
        assert_eq!(
            Track::transform_from_corners(
                (0.0, 0.0), (10.0, 10.0),
                Rectangle {
                    top_left: Point { x: 0, y: 0 },
                    size: Size { width: 100, height: 50 }
                },
            ),
            ((0.0, 0.0), 5.0)
        );
    }
    #[test]
    fn ugh() {
        let t = Track::new(
            (0.0, 0.0), (10.0, 10.0),
            Rectangle {
                top_left: Point { x: 0, y: 0 },
                size: Size { width: 100, height: 50 }
            }
        );
        assert_eq!(t.transform.1, 5.0);
    }
    
    #[test]
    fn same() {
        let bounds = Rectangle {
            top_left: Point { x: 0, y: 0 },
            size: Size { width: 100, height: 100 }
        };
        let mut t = Track::new(
            (10.0, 10.0), (10.0, 10.0),
            bounds,
        );
        t.add_resize(20.0, 20.0, bounds);
        assert!(t.transform.1 <= 10.0)
    }
    
    #[test]
    fn prune() {
        let bounds = Rectangle {
            top_left: Point { x: 0, y: 0 },
            size: Size { width: 100, height: 100 }
        };
        let mut t = Track::new(
            (10.0, 10.0), (10.0, 10.0),
            bounds,
        );
        t.add_resize(20.0, 20.0, bounds);
        assert!(t.transform.1 <= 10.0)
    }
}