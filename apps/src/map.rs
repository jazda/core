use crate::{Display, Gnss};
use crate::geo::Track;

use core::fmt::Debug;
use libm;
use ray_graphics::{PixelStore, Primitive, TiledRenderer, TwoLevel};
use ray_graphics::dround as digits;
use ray_graphics::egc::{
    geometry::Dimensions,
    prelude::{Point, Size},
    primitives::Rectangle,
};
use ray_graphics::sdf::{scale, translate};
use sentencer::CopySentencer;
use yanp::parse::{GpsPosition, SentenceData};

use ray_graphics::Monochrome;

#[cfg(not(feature="std"))]
/// lolremez --float -d 3 -r -180:180 'cos(x * tau / 360)'
fn geo_stretch(x: f32) -> f32 {
    let u = -2.9736394e-18f32;
    let u = u * x + -6.1728395e-5f32;
    let u = u * x + 4.3593817e-14f32;
    u * x + 6.9436324e-1f32
}
#[cfg(feature="std")]
fn geo_stretch(inclitude: f32) -> f32 {
    (inclitude * core::f32::consts::TAU / 360.0).cos()
}

/// Projects geographic distance to "degrees".
///
/// Will totally fail near Equator, Greenwich (fix the position representation), as well as date zone (do wrapping based on a reference point) and near poles (this is actually difficult)
fn _project(p: GpsPosition) -> (f32, f32) {
    // finallly, some meaningful names
    let spinitude = p.lon;
    let inclitude = p.lat;
    let incl_dist_factor = 1.0;
    // this is somewhat expensive but done once a second
    let spin_dist_factor = geo_stretch(inclitude);
    // pictures get drawn with y increasing downwards,
    // but maps with inclinitude increasing upwards (North)
    (spinitude * spin_dist_factor, -inclitude * incl_dist_factor)
}

/// Our differences are small; try to avoid running out of precision.
fn project(p: GpsPosition) -> (f32, f32) {
    let (spin, incl) = _project(p);
    (spin * 4096.0, incl * 4096.0)
}

enum Multi {
    Empty,
    One((f32, f32)),
    Track(Track),
}


macro_rules! bake {
    ($f:expr => $($arg:expr),* $(,)?) => {
        move |point: ray_graphics::egc::prelude::Point| {
            $f(point $(,$arg)*)
        }
    }
}

fn to_dark(v: bool) -> Monochrome {
    if v { Monochrome::Dark }
    else { Monochrome::Light }
}

/// ./lolremez --float -d 4 -r 0.001:2 'sqrt(x)'
fn sqrt(x: f32) -> f32 {
    libm::sqrtf(x)
}

fn dist_euclid((x, y): (f32, f32), (x1, y1): (f32, f32)) -> f32 {
    let a = x - x1;
    let b = y - y1;
    sqrt(a * a + b * b)
}

pub fn run<
    E: Debug,
    G: Gnss<Error=E>,
    D: Display<Error=E> + Dimensions + PixelStore<Monochrome> + TiledRenderer<Color=Monochrome>
>(mut gnss: G, mut display: D) -> Result<(), E>
    where <D as TiledRenderer>::Error: Debug
{
    let mut gbuf = [0; 64];

    let mut sentencer = CopySentencer::new();
    // track calculation
    let mut track = Multi::Empty;
    // distance calculation
    // position in projected degrees
    let mut last_position = None;
    // Euclidean between fake degrees.
    // Assuming that the distances between consecutive points will never grow so high that curvature has any great meaning.
    // And if they do, it's a glich, and garbage in->garbage out.
    let mut distance_4kdeg = 0.0;
    
    {
        display.set_area(display.bounding_box(), Monochrome::Light);
        display.flush()?;
    }
    
    loop {
        let (read_count, ret) = gnss.read(&mut gbuf);
        ret?;
        let raw = &gbuf[..read_count as usize];

        sentencer.add(raw).unwrap();
        for s in sentencer.read().unwrap() {
            let result = yanp::parse_nmea_sentence(s);
            match result {
                Ok(SentenceData::RMC(s)) => {
                    if let Some(s) = s.position {
                        let position = project(s);
                        (distance_4kdeg, last_position) = match last_position {
                            None => (0.0, Some(position)),
                            Some(last_position) => (
                                distance_4kdeg + dist_euclid(last_position, position),
                                Some(position),
                            )
                        };

                        let bounds = Rectangle {
                            top_left: Point { x: 0, y: 0},
                            size: Size { width: 150, height: 150 },
                        };
                        if let Multi::Track(ref mut track) = track {
                            track.add_resize(position.0, position.1, bounds);
                        } else if let Multi::One(start) = track {
                            track = Multi::Track(Track::new(start, position, bounds));
                        } else {
                            track = Multi::One(position);
                        }
                        
                        if let Multi::Track(track) = &track {
                            let scene = TwoLevel {
                                background: Primitive::Light,
                                foreground: (bounds, track),
                            };
                            scene.draw(&mut display);
                        }
                    }
                    let time_got = Rectangle {
                        top_left: Point { x: 150, y: 0},
                        size: Size { width: 26, height: 13 },
                    };
                    let color = if let Some(t) = s.time {
                        if t.second % 2 == 0 { Monochrome::Light }
                        else { Monochrome::Dark }
                    } else {
                        Monochrome::Light
                    };
                    display.set_area(time_got, color);
                    
                    use ray_graphics::Fixed;
                    const EARTH_RADIUS_KM: i32 = 6371;
                    const EARTH_CIRCUMFERENCE_KM: Fixed = Fixed::TAU.mul(EARTH_RADIUS_KM);
                    const DEGREE_KM: Fixed = EARTH_CIRCUMFERENCE_KM.div(360);
DEGREE_KM.round();
                    let distance_tensofm = DEGREE_KM.mulf32(distance_4kdeg) * 100.0 / 4096.0;
                    let mut dt = distance_tensofm as u32;

                    let mut d = [10; 5];
                    for i in (0..5usize).rev() {
                        d[i] = (dt % 10) as u8;
                        dt = dt / 10;
                        if dt == 0 { break }
                    }

                    let sdf = bake!(digits::n => &d[..]);
                    let top_left = Point { x: 0, y: 150};
                    let sdf = bake!(scale => sdf, Fixed::one().mul(64).div(26));
                    let sdf = bake!(translate => sdf, top_left);
                    display.fill(
                        &Rectangle {
                            top_left,
                            size: Size { width: 150, height: 26 },
                        },
                        ray_graphics::F(|x| to_dark(sdf(x))),
                    ).unwrap();
                    
                    display.flush()?;
                },
                _ => {},
            }
        }
    }
}